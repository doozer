# = Doozer
# 
# Doozer is a simple fixture generation library/plugin that handles all the annoying id management, and association building, allowing you to focus on crafting meaningful test fixtures.
# 
# Here's a simple Doozer definition script:
# 
#   Doozer.define :page do
#     field_order   :title, :content, :author
#     defaults      :content=>'Content Here', :author=>'System', :created_on=>Time.now
#   end
# 
# That tells Doozer that you are going to generate `page` fixtures. It then generates three macros for you to use: `page`, `page_id`, `pages`. With those macros, you can define your test data like this:
# 
#   page :root, "Home Page" do
#     page :news, "News is always Fun!" do
#       page :site_launch, "Site launched!", :created_on=>1.week.ago
#       page :more_news, "More news", :created_on=>3.days.ago
#     end
#   end
# 
# You can reference the same fixture multiple times, so you can create partial fixtures and complete them elsewhere. 
# 
# In our example so far, all of the pages will have the default content of "Content Here". Let's change that by calling the `page` macro again with just the content attribute. These will be added to the bottom of the definition file:
# 
#   page :root, :content=><<-EOC
#   Welcome to this this site, please enjoy yourself!
#   EOC
#   
#   page :news, :content=>"I'm the parent page to all of the news!"
#   page :site_launch, :content=>"Really, it was!"
# 
#   # So on, and so forth
# 
# That's it for the definitions... 
# 
# Run `rake doozer:build` to generate the fixtures and you're done. 
# 
# Enjoy!

require 'rubygems'
require 'active_support'
require 'yaml'

require 'doozer/definition'
require 'doozer/alt_definition'
require 'doozer/builder'
require 'doozer/version'
