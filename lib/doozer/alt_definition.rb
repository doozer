require 'active_support'

module Kernel
  def as
    :as
  end
end

def doozer(name, opts={}, &block)
  # Load up the settings from the alternate syntax
  altdef = Doozer::AltDefinition.new(name, opts)
  altdef.instance_eval(&block) if block_given?
  
  # Redefine in official definition format... kinda
  Doozer.define( name, opts )
  obj_list = Doozer.object_list[name]
  obj_list[:field_order] = altdef.field_definitions.field_list unless altdef.field_definitions.field_list.empty?
  obj_list[:default_values] = altdef.field_definitions.field_defaults
  obj_list[:block_affects] = altdef.block_affects unless altdef.block_affects.nil?
  obj_list[:before_generate] = altdef.event_handlers[:generate] if altdef.event_handlers.has_key?(:generate)
  obj_list[:target] = altdef.target_name unless altdef.target_name.nil?
end

module Doozer
  class AltDefinition
    attr_reader :name, :options, :field_definitions, :block_affects, :event_handlers, :target_name
    
    def initialize(name, opts={})
      @name = name
      @options = opts
      @field_definitions = CleanFields.new
      @block_affects = nil
      @target_name = nil
      @event_handlers = {}
    end
    
    def fields(&block)
      @field_definitions.instance_eval(&block)
    end
    
    def children(action)
      if action == :belong_to
        @block_affects ||= []
        @block_affects << {:child=>:belongs_to}
      elsif action == :act_as_tree or action == :parent_id
        @block_affects ||= []
        @block_affects << {:child=>:parent_id}
      end
    end

    def parent(action)
      if action == :belongs_to
        @block_affects ||= []
        @block_affects << {:parent=>:belongs_to}
      end
    end
    
    def target(target_name)
      @target_name = target_name
    end
    
    def on(key, &block)
      @event_handlers[key] = block if block_given?
    end
    
  end
  
  class CleanFields
    attr_reader :field_list, :field_defaults
    def initialize
      @field_list = []
      @field_defaults = {}
    end
    def id(*args) method_missing(:id, *args); end
    def name(*args) method_missing(:name, *args); end
    def type(*args) method_missing(:type, *args); end
    def method_missing(name, *args)
      name = name.id2name
      @field_list << name.to_sym
      @field_defaults[name.to_sym] = args[0] if args.length > 0
    end
  end  
end