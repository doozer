require 'rake'

namespace :doozer do

  desc "Set the Doozers to work generating fixtures"
  task :build=>[:environment] do

    # Add the base path so that doozer can load other doozers
    $: << File.join(RAILS_ROOT, 'test', 'fixtures')

    # Load all the doozers!
    Dir[ File.join(RAILS_ROOT, 'test', 'fixtures', 'doozers', '*.rb') ].each do |doozer|
      require doozer
    end
    
    Doozer::Builder.write_fixtures(File.join(RAILS_ROOT, "test", "fixtures"), :verbose=>true)
    
  end

end