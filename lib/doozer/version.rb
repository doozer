# Version 0.1.1
module Doozer
  module VERSION 
    MAJOR = 0
    MINOR = 1
    TINY  = 1

    STRING = [MAJOR, MINOR, TINY].join('.')
  end
end