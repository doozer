# Experimental
module Doozer 
  class << self
    def load_doozers(fixture_path=nil, doozer_path=nil) 
      fixture_path ||= File.expand_path(File.join(RAILS_ROOT, 'test', 'fixtures'))
      doozer_path  ||= File.expand_path(File.join(fixture_path, 'doozers'))
      
      # Add fixtures to the load path so that doozers can require other doozers
      $: << fixture_path
      # Load all the doozers!
      Dir[ File.join(doozer_path, '*.rb') ].each do |doozer|
        require doozer
      end
    end
  end
end