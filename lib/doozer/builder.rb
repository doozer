module Doozer
  module Builder
    class << self
      
      def build_output_for_type(obj_type) #nodoc#
        output =  "# This fixture was generated with a Doozer. You can find it's definition\n"
        output << "# script under test/fixtures/doozers/\n# \n"
        output << "# Generated on #{Time.now.to_s :long}\n\n"
        obj_def = Doozer.object_list[obj_type]
        
        # We want to make sure and create the fixtures in order (id)
        obj_def[:row_names].each_with_index do |name, i|
          data_row = obj_def[:data_rows][name]
          obj_def[:before_generate].call(data_row) if obj_def.has_key?(:before_generate)
          item = {}
          item[name.to_s] = data_row.stringify_keys
          yml_parts = item.to_yaml.split("\n")
          yml_parts.shift # Get rid of the initial '---' stuff ....
          yml = yml_parts.join("\n").to_s
          yml += "\n\n"
          output << yml
        end
        
        output
      end
      
      # This writes out YAML fixtures to the specified path. The path must be a directory
      # or this will raise a RuntimeError
      def write_fixtures(path, options={})
        full_path = File.expand_path(path)
        options.reverse_merge!({:verbose=>false})
        raise "#{full_path} is not a directory" unless FileTest.directory?( full_path )

        print "Doozers are generating fixtures: " if options[:verbose]

        Doozer.object_list.keys.each do |name|
          File.open( File.join(full_path, "#{name.to_s.pluralize}.yml"), 'w' ) do |f|
            f.write build_output_for_type(name)
            print "#{name.to_s.pluralize}.yml, " if options[:verbose]
          end
        end
        puts "Done." if options[:verbose]
      end
      
    end
  end
  
  class << self
    def write_fixtures(path, options={})
      path ||=  File.expand_path(File.join(RAILS_ROOT, 'test', 'fixtures'))
      Doozer::Builder.write_fixtures(path, *options)
    end
  end
end
