require File.dirname(__FILE__) + '/../test_helper'
require 'doozer'
require 'doozer/alt_definition'

require 'pp'

class AltDefinitionTest < Test::Unit::TestCase

  def setup
    Doozer.reset!
  end

  should "generate fixture definition data structure" do # ---------------
    
    doozer :user
    
    doozer :page do 
      fields {  # Defaults
        title     'My Title'
        content
        author 
      }  
    end
    
    expected_structure = {
      :user=>{ 
        :target=>:user,
        :row_names=>[],
        :data_rows=>{},
        :default_values=>{},
        :block_affects=>[{:child=>:belongs_to}]
      },
      :page=>{ 
        :target=>:page,
        :default_values=>{:title=>"My Title"},
        :block_affects=>[{:child=>:belongs_to}],
        :field_order=>[:title, :content, :author],
        :row_names=>[],
        :data_rows=>{}
      }
    }
    
    assert_equal expected_structure, Doozer.object_list
  end

  should "generate helper methods" do # ----------------------------------
    
    doozer :page
    
    assert_not_nil defined?(page)
    assert_not_nil defined?(page_id)
    assert_not_nil defined?(page_name)
    assert_not_nil defined?(pages)

    doozer :person
    
    assert_not_nil defined?(person)
    assert_not_nil defined?(person_id)
    assert_not_nil defined?(person_name)
    assert_not_nil defined?(people)
  end
  
  should "return next object id" do # ------------------------------------

    doozer :user

    assert_equal 1, Doozer.next_object_id( :user )
    user :bob, :name=>'Bob'
    assert_equal 2, Doozer.next_object_id( :user )
  end
  
  should "add objects to data pool from named arguments" do # ------------

    doozer :user
    
    user :bob, :name=>'Bob'
    assert_equal 1, user_id(:bob)
    assert_equal "Bob", users(:bob)[:name]

    user :name=>'Matt'
    user :name=>'Dan'

    assert_equal 3, user_id(:user_3)
    assert_equal 'Dan', users(:user_3)[:name]
  end
  
  should "add objects to data pool from positional arguments" do # -------

    doozer :user do
      fields { name } 
    end
    
    user :bob, 'Bob'
    assert_equal 1, user_id(:bob)
    assert_equal "Bob", users(:bob)[:name]

    user :matt, 'Matt'
    user 'Dan'

    assert_equal 3, user_id(:user_3)
    assert_equal 'Dan', users(:user_3)[:name]
  end
  
  should "add default values to data objects" do # -----------------------
    created_on_time = Time.now
    
    doozer :page do
      fields {    # DEFAULTS: 
       title        'New Title'
       author       'System'
       content      'Content'
       created_on   created_on_time
       slug         'Slugline'
      }
    end
    
    page "A beginning"
    page 
    page "Stuff", "Junk", "And more!"

    assert_equal 3, Doozer.object_list[:page][:data_rows].keys.length
    assert_equal created_on_time, pages(:page_1)[:created_on]
    assert_equal "Slugline", pages(:page_1)[:slug]
    assert_equal "A beginning", pages(:page_1)[:title]
  end
  
  should "allow multiple updates to a single named fixture" do # ---------
    
    doozer :car do
      fields { 
        name      'CAR'
        type      'TYPE'
        year      'YEAR'
      }
    end
    
    car :fast
    assert_equal 1,      cars(:fast)[:id]
    assert_equal 'CAR',  cars(:fast)[:name]
    assert_equal 'TYPE', cars(:fast)[:type]
    assert_equal 'YEAR', cars(:fast)[:year]
    
    car :fast, 'Corvette'
    car :fast, :type=>'Sportscar'
    car :fast, :year=>2002
    
    assert_equal 'Corvette',  cars(:fast)[:name]
    assert_equal 'Sportscar', cars(:fast)[:type]
    assert_equal 2002,        cars(:fast)[:year]


    # Last one in should win
    car :fast, 'Tiburon'
    assert_equal 'Tiburon', cars(:fast)[:name]

    # Positional params 'beat' named params
    car :fast, 'Viper', :name=>'Porche'
    assert_equal 'Viper', cars(:fast)[:name]
    
    # Shouldn't wind up with multiple :fast keys in object list
    assert_equal [:fast], Doozer.object_list[:car][:row_names]
  end

  should "be able to override the target AR class" do # ------------------
    
    doozer :page do
      target 'Comatose::Page'
    end
    
    assert_equal 'Comatose::Page', Doozer.object_list[:page][:target]
    
  end
  
  should "support block association definition" do # ---------------------

    doozer :node do
      fields   { name }
      children :act_as_tree
    end
    
    assert_equal Doozer.object_list[:node][:block_affects], [{:child=>:parent_id}]
  end
  
  should "correctly nest fixtures" do # ----------------------------------

    doozer :node do
      fields   { name }
      children :act_as_tree
    end

    node "Root Node" do
      node "First Child" do
        assert_equal 2, Doozer.object_stack.length
        node "Sub Child 1"
        node "Sub Child 2"
      end
      node "Second Child"
    end
    
    assert_equal 1, nodes(:node_2)[:parent_id]
    assert_equal 2, nodes(:node_3)[:parent_id]
    assert_equal 2, nodes(:node_4)[:parent_id]
  end
  
  should "allow macro aliases" do # --------------------------------------
    doozer :node, as=>:n do
      fields   { name }
      children :act_as_tree
    end
    
    assert_not_nil defined?(n)
    assert_not_nil defined?(n_id)
    assert_not_nil defined?(ns)

    n "Root Node" do
      n "First Child" do
        n "Sub Child 1"
        n "Sub Child 2"
      end
      n "Second Child"
    end
    
    assert_equal 1, ns(:node_2)[:parent_id]
    assert_equal 2, ns(:node_3)[:parent_id]
    assert_equal 2, ns(:node_4)[:parent_id]
  end
  
  should "allow alternate block syntax" do # -----------------------------
    doozer(:node, as=>:n){
      fields   {name}
      children :act_as_tree
    }
    
    n("Root Node") { 
      n("First Child") {
        n "Sub Child 1"
        n "Sub Child 2"
      }
      n "Second Child"
    }
    
    assert_equal 1, ns(:node_2)[:parent_id]
    assert_equal 2, ns(:node_3)[:parent_id]
    assert_equal 2, ns(:node_4)[:parent_id]
    
    expected_row_data = {
       :node_1=>{:name=>"Root Node", :id=>1},
       :node_2=>{:parent_id=>1, :name=>"First Child", :id=>2},
       :node_3=>{:parent_id=>2, :name=>"Sub Child 1", :id=>3},
       :node_4=>{:parent_id=>2, :name=>"Sub Child 2", :id=>4},
       :node_5=>{:parent_id=>1, :name=>"Second Child", :id=>5}
    }
    
    assert_equal expected_row_data, Doozer.object_list[:node][:data_rows]
  end
  
  should "allow `belongs_to` relationships" do # --------------------------
    
    doozer :page do 
      fields { title } 
      parent :belongs_to
    end
    doozer :user do
      fields    { name; email }
      children  :belong_to
    end
    doozer :comment do
      fields { body }
      # Use the default block_affects, which should be `block_affects :child=>:belongs_to`
    end
    
    # Data definitiions:
    
    page :welcome, 'Welcome' do
      user :matt, 'Matt' do
        comment :matt_first_comment, 'The beginning'
      end
    end
    
    assert_equal user_id(:matt), pages(:welcome)[:user_id]
    assert_equal user_id(:matt), comments(:matt_first_comment)[:user_id]
  end
  
  should "remove macro names on `reset!`" do # ---------------------------
    doozer :page
    
    assert_not_nil defined?(page)
    assert_not_nil defined?(page_id)
    assert_not_nil defined?(page_name)
    assert_not_nil defined?(pages)
    
    Doozer.reset!

    assert_nil defined?(page)
    assert_nil defined?(page_id)
    assert_nil defined?(page_name)
    assert_nil defined?(pages)
  end
  
  should "allow access to the fixture name and data and from `ID`" do # --
    doozer :page do
      fields { title }
    end
    
    page :one,   "Page One"
    page :two,   "Page Two"
    page :three, "Page Three"
    
    assert_equal :one, page_name( page_id(:one) )
    assert_equal :two, page_name( page_id(:two) )
    
    assert_equal 'Page One',   pages( page_id(:one) )[:title]
    assert_equal 'Page Three', pages( page_id(:three) )[:title]
  end
end
