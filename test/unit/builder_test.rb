require File.dirname(__FILE__) + '/../test_helper'
require 'doozer'

require 'pp'

class BuilderTest < Test::Unit::TestCase
  
  def setup
    Doozer.reset!
    created_on_time = Time.now.to_s(:db)
    
    Doozer.define :user do 
      field_order :name, :email, :password
      defaults    :password=>'???', :created_on=>created_on_time
      before_generate do |atts|
        begin
          atts[:login] = Doozer.object_list[:user][:row_names][atts[:id]-1].to_s unless atts.has_key?(:login)
        rescue
          puts "Oops"
        end
      end
    end
    
    user :bob, "Bob", "bob@yahoo.com", "Bobo"
    user "Other", "other@gmail.com"  
  
    @created_on_time = created_on_time
  end

  should "generate output yaml string" do # ------------------------------

    out = Doozer::Builder.build_output_for_type(:user)
    data = YAML::load(out)

    expected_data = {
      'bob'=>{ 
        'id'=>1,
        'login'=>'bob',
        'name'=>'Bob', 
        'email'=>'bob@yahoo.com', 
        'password'=>'Bobo',
        'created_on'=>@created_on_time
      },
      'user_2'=>{
        'id'=>2,
        'login'=>'user_2',
        'name'=>'Other',
        'email'=>'other@gmail.com',
        'password'=>'???',
        'created_on'=>@created_on_time
      }
    }
    
    assert_equal expected_data, data
  end
  
  should "generate yaml fixture files" do # ------------------------------
    
    Doozer.define :node do
      field_order   :name
    end

    node "Root Node" do
      node "First Child" do
        node "Sub Child 1"
        node "Sub Child 2"
      end
      node "Second Child"
    end
    
    assert_nothing_raised(RuntimeError) do
      Doozer::Builder.write_fixtures(File.join(File.dirname(__FILE__), '..', 'fixtures'))
    end
    assert_raise(RuntimeError) do
      Doozer::Builder.write_fixtures(File.join(File.dirname(__FILE__), "crap"))
    end
  end

end
