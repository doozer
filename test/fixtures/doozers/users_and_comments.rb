require 'doozer'

doozer :user do 
  fields {
    name
    email 
    password    "???"
    created_on  Time.now.to_s(:db)
  } 
  on :generate do |atts|
    atts[:login] = Doozer.object_list[:user][:row_names][atts[:id]-1].to_s unless atts.has_key?(:login)
  end
end

doozer :comment do
  fields {
    title
    body
    author      'unknown'
    created_on  Time.now.to_s(:db)
  }
  on :generate do |atts|
    atts[:author] = users(atts[:user_id])[:name] if att.has_key? :user_id
  end
end

user :bob, "Bob", "bob@yahoo.com", "Bobo" do
  comment 'Good question', 'Why would anyone care?'
end

user "Other", "other@gmail.com" do
  comment 'RE: Good question', 'I know you are but what am I?!'
  comment 'RE: RE: Good question', 'Oh sorry, my inner second-grader came out there.'
end
