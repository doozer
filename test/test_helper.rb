require 'test/unit'

class Test::Unit::TestCase

  def assert_difference(object, method = nil, difference = 1)
    initial_value = object.send(method)
    yield
    assert_equal initial_value + difference, object.send(method), "#{object}##{method}"
  end

  def assert_no_difference(object, method, &block)
    assert_difference object, method, 0, &block
  end

  class << self
    def should(behave,&block)
      method_name = "test_should_#{behave.gsub(' ', '_')}"
      if block
        define_method method_name, &block
      else
        puts ">>> Untested: #{name.sub(/Test$/,'')} should #{behave}"
      end
    end
  end
end