    From:                     To:
    -------------------------=----------------------------
     node_1:                 | Doozer.define :node do
         id: 1               |   field_order :name
         parent_id:          |   block_affects :child=>:parent_id
         name: Root Node     | end
                             | node "Root Node" do
     node_2:                 |   node "First Child" do
         id: 2               |     node "Sub Child 1"
         parent_id: 1        |     node "Sub Child 2"
         name: First Child   |   end
                             |   node "Second Child"
     node_3:                 | end
         id: 3               | 
         parent_id: 2        | 
         name: Sub Child 1   | 
                             | 
     node_4:                 | 
         id: 4               | 
         parent_id: 2        | 
         name: Sub Child 2   | 
                             | 
     node_5:                 | 
         id: 5               | 
         parent_id: 1        | 
         name: Second Child  | 

'Nuff said!