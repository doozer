
## Builder should: 


* generate output yaml string
* generate yaml fixture files


## Definition should: 


* generate fixture definition data structure
* generate helper methods
* return next object id
* add objects to data pool from named arguments
* add objects to data pool from positional arguments
* add default values to data objects
* allow multiple updates to a single named fixture
* be able to override the target AR class
* support block association definition
* correctly nest fixtures
* allow macro aliases
* allow alternate block syntax
* allow `belongs_to` relationships
* readme example


