require 'rake'
require 'rake/testtask'
$: << 'lib'
require 'doozer/version'
require 'doozer/tasks/fixed_hoe'
require 'doozer/tasks/specs'
require 'doozer/tasks/scm'

FixedHoe.new('doozer', Doozer::VERSION::STRING) do |p|
  p.rubyforge_name = 'doozer'
  p.author = "M@ McCray"
  p.email = 'darthapo@gmail.com'
  p.summary = 'A test fixture generator'
  p.description = p.paragraphs_of('ReadMe', 1..2).join("\n\n")
  p.url = 'http://www.mattmccray.com'
  p.changes = p.paragraphs_of('History', 0..1).join("\n\n")
  p.extra_deps << 'active_support'
end

desc 'Run the default tasks'
task :default => :test


require 'spec/rake/spectask'

desc "Run all examples"
Spec::Rake::SpecTask.new('spec') do |t|
  t.spec_files = FileList['specs/**/*.rb']
end

desc "Generate specdocs for examples for inclusion in RDoc"
Spec::Rake::SpecTask.new('specdoc') do |t|
  t.spec_files = FileList['specs/**/*.rb']
  t.spec_opts = ["--format", "rdoc:EXAMPLES.rd"]
end

Rake::TestTask.new do |t|
  t.libs << "lib"
  t.test_files = FileList['test/*/*test.rb']
  t.verbose = false
end

Behaviors::ReportTask.new :specs do |t|
  t.pattern = 'test/**/*_test.rb'
  t.html_dir = 'docs'
  t.src_dir = 'docs/src'
end

desc "Generates the documentation"
task :doc=>[:specs_src] do
  require 'bluecloth'
  require 'erb'
  require 'active_support'
  #RedCloth::DEFAULT_RULES.replace [:markdown, :textile]
  
  src_files = FileList['docs/src/*.markdown']
  src_filenames = src_files.map {|sf| File.basename sf, '.markdown' }
  src_titles = src_filenames.map {|sfn| sfn.gsub('_', ' ').titleize }
  layout_tmpl = ERB.new(File.readlines('docs/src/_layout.rhtml', 'r').join(''))

  puts "#{src_files.length} docs found.\n"  

  src_files.each do |src|
    filename = File.basename src, '.markdown'
    title = filename.gsub('_', ' ').titleize
    content = BlueCloth.new(File.readlines(src, 'r').join('')).to_html
    page_html = layout_tmpl.result(binding)
    File.open("docs/#{filename}.html", 'w' ) do |f|
      f.write( page_html )
    end
  end
  
  puts "Done."
end

